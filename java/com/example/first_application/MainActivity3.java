package com.example.first_application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        TextView Name1=findViewById(R.id.Name1);
        TextView Name2=findViewById(R.id.Name2);
        TextView email1=findViewById(R.id.Email1);
        TextView password1=findViewById(R.id.password1);
        TextView phone1=findViewById(R.id.phone1);
        TextView campus=findViewById(R.id.campus);
        TextView Address1=findViewById(R.id.address1);
        TextView batch=findViewById(R.id.batch);
        TextView degree=findViewById(R.id.degree);
        Button exit=findViewById(R.id.Exit);
        batch.setText("18");
        degree.setText("CS");
        campus.setText("Faislabad Chionut");
        String Username=getIntent().getStringExtra("Name");
        String Username1=getIntent().getStringExtra("Name1");
        String Email2=getIntent().getStringExtra("Email");
        String password2=getIntent().getStringExtra("password");
        String phone2=getIntent().getStringExtra("phone");
        String address2=getIntent().getStringExtra("address1");
        Name1.setText(Username);
        Name2.setText(Username1);
        email1.setText(Email2);
        password1.setText(password2);
        phone1.setText(phone2);
        Address1.setText(address2);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity3.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
}