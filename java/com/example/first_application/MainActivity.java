package com.example.first_application;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View mainView = findViewById(R.id.main);
        EditText email = findViewById(R.id.Email);
        EditText password = findViewById(R.id.password);
        Button button = findViewById(R.id.button);
        TextView textView=findViewById(R.id.signup);
        String Email2=getIntent().getStringExtra("Email");
        String password2=getIntent().getStringExtra("password");
        email.setText(Email2);
        password.setText(password2);
        String email3=email.getText().toString();
        String password3=password.getText().toString();
       if(Email2==email3 && password3==password2 ) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, "Email And Password are Equal", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CreateAlertDialoge();
                }
            });
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity3();
            }
        });



                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openActivity2();
                    }
                });

        
    }
    private void CreateAlertDialoge(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Email And Password not Equal");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Yes Button is pressed", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "No Button is pressed", Toast.LENGTH_SHORT).show();
            }
        });
        builder.create();
        builder.show();
    }


    
    
    public  void openActivity2(){
        Intent intent =new Intent(this,MainActivity2.class);
        startActivity(intent);
        Toast.makeText(MainActivity.this, "You Clicked Sign up Button", Toast.LENGTH_LONG).show();

    }
    public void openActivity3()
    {
        Intent intent=new Intent(this,MainActivity3.class);
                startActivity(intent);
        Toast.makeText(MainActivity.this, "You clicked on Sign in Button ", Toast.LENGTH_SHORT).show();
    }

}